/* 1. write js function to handle the SUT 
 * 2. build test suite using excel 
 * 3. perform test execution
 * 4. perform test evaluation
 * EXTRA. write a function that will execute the test suite 
 */

function SUT_Test(a, b){
 return a + b;    
}

function test_Execution(){
    console.log("T1: Actual result = ", SUT_Test(1, 2))
    console.log("T2: Actual result = ", SUT_Test(11, 4))
    console.log("T3: Actual result = ", SUT_Test(2, 2))
    console.log("T4: Actual result = ", SUT_Test(-6, -7))
}
// Invoking the function
test_Execution()

