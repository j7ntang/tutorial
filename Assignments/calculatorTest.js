/**
 * create a calculator
 * run a test
 * the tripple A (arrange, act, )
 */

const calculator = require("calculator.js")

class calculatorTest{
    constructor(sut){
        this._sut = sut
    }
    addition(x,y){
        //arrange
        const z = x+y
        console.log(this._sut.addition() === z ? "passed" : "failed")
    }
}