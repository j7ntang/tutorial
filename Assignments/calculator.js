/**
 * Build a calculator class that has the the following properties
 * color
 * size
 * The following functionalities - method
 * Addition
 * subtraction 
 * multiplication
 * division
 * Squareroot
 * Square
 * Remainder
 * getters and setters to get the properties of a class
 * 
 */

class Calculator{
    constructor(firstNumber, secondNumber, color, size){
        this._firstNumber = firstNumber
        this._secondNumber = secondNumber
        this._color = color
        this._size = size
    }
    set firstNum(x){
        this._firstNumber = x
    }
    get firstNum(){
        return this._firstNumber
    }

    set secondNum(y){
        this._secondNumber = y
    }
    get secondNum(){
        return this._secondNumber
    }
    set color(colortext){
        this._color = colortext
    }
    get color(){
        return this._color
    }
    set size(sizetext){
        this._size = sizetext
    }
    get size(){
        return this._size
    }
    addition(){
        return this._firstNumber + this._secondNumber
    }
    subtraction(){
        return this._firstNumber - this._secondNumber
    }
    multiplication(){
        return this._firstNumber * this._secondNumber
    }
    division(){
        return this._firstNumber / this._secondNumber
    }
    squareroot(){
        return (`${(this._firstNumber) **0.5} ${(this._secondNumber) **0.5}`)
    }
    square(){
        return (`${(this._firstNumber) **2} ${(this._secondNumber) **2}`)
    }
    remainder(){
        return this._firstNumber % this._secondNumber
    }

}

let calculator = new Calculator(16, 9, "red", "large")
// calculator.firstNum = 7
// calculator.secondNum = 9

console.log(calculator.squareroot(),calculator.size)
console.log(`${calculator.square()} ${calculator.color}`)