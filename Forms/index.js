const form = document.getElementById("form");

form.addEventListener("submit", (e) => {
    // prevent form submission
    e.preventDefault();

    let date = form.elements['date'].value
    let message = form.elements['message'].value
    let vehicle = form.elements['vehicle'].value
    let email = form.elements['email'].value

    console.log(`Hi, ${email.split('@')[0]} on the ${date} your email is ${email} vehicle type is ${vehicle}. The message is '${message}'`)
    })