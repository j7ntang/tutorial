/*
Boolean
Array
Object
String
Number
null
undefined
NaN
*/

let foods = ["carbohydrates", "fruits", "fats", "vegetables", "dairy", "protein"]

function displayFoods(groups){
    foodText = ""
    for(let group of groups){
        foodText += group + " "
    }
    return foodText
}
// console.log(displayFoods(foods))


let carbohydrate = {name: "Bread", weight: 10, weightUnit: "kilograms"}

function displayCarb(){
    carbText = ""
    for(let info in carbohydrate){
        carbText += carbohydrate[info] + " "
    }
    return carbText
}
console.log(displayCarb(carbohydrate))