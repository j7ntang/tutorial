// Define a variables
let fullName;
var firstName;

// const must be initialized when defined
const LASTNAME = "Brown"

// initializing a variable is to give it an initial value
// initialising name
fullName = "Oscar Praat";

// console.log(fullName);

// String concatenation
// console.log(LASTNAME + " " + fullName);
// console.log(`${LASTNAME} ${fullName}`);

// function printName(times){
//     for(let x = 1; x <= times; x++){
//         console.log(fullName)
//     }
// }

// printName(10)

const CARBS = ["pizza", "bread", "cookie"]
CARBS.reverse().push("Jesse")
CARBS.pop()
console.log(CARBS)