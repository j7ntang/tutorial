const root = document.getElementById("root")

// creating an h1 element
const galleryTitle = createElementWithId("h1", "galleryTitle")

// putting in text into the element
galleryTitle.innerText = "This is the gallery"

const gallery = createElementWithId("div", "gallery")
const image = createElementWithId("img", "newImage")
image.src = "france-in-pictures-beautiful-places-to-photograph-eiffel-tower.jpg"
gallery.appendChild(image)

// appending the element to the root
root.appendChild(galleryTitle)
root.appendChild(gallery)

root.style.backgroundColor = "yellow"

function createElementWithId(elementTag, idName){
    const elm = document.createElement(elementTag)
    elm.id = idName
    return elm
}