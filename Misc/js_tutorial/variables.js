// Single line comment

/*
This
is
a
multiline
comment
*/

// Quick commenting - ctrl + /

/*
    Creating variables
    1. var - global scope
    2. let - local scope
    3. const - local constant
*/

var greeting = 'Hello guys';
// greeting = 5;

let x = 4;
let y = 5;
const z = x + y;

// console.log(z + x + '' + y);
// backtick
console.log(`Hello people ${z}${x}${x}`);



