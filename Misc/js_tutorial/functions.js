function sayHello(name) {
    return "Hello " + name;
}

// Invoke or call a function
// console.log(sayHello(007));

let person = {
    name: "James",
    age: 98,
    height: 1.87,
    gender: "male"
}

function getName(){
    return person.name;
}

function setName(name){
    person.name = name;
}

// Write your code here
setName("Elsie")

console.log(getName());
