/**
 * write a function that takes someones first name, last name and age. if they are 18 and
 * above write a message to welcome them. if below 18, disqualify them politely.
 */
function userInfo(firstName, lastName, age) {
  let message = "";
  if (age >= 18) {
    message = `Welcome ${firstName} ${lastName}.`;
  } else if (age < 18 && age >= 0) {
    message = `Sorry ${firstName} ${lastName}, you are not eligible`;
  }
  return message;
}
console.log(userInfo("kiven", "valerie", -5))