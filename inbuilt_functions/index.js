class Person{
    constructor(){
    }
}

let person = new Person()

// console.log(person instanceof Number)
// console.log(typeof "")

function sayHi(name){
    return "hello " + name
}

let sayHi = (name) => "hello " + name

console.log(sayHi('Elvira'))